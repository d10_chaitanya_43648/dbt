System Database - information_schema


SELECT ROUTINE_NAME, ROUTINE_TYPE, ROUTINE_DEFINITION, DEFINER FROM ROUTINES;

SELECT ROUTINE_NAME, ROUTINE_TYPE, ROUTINE_DEFINITION, DEFINER FROM ROUTINES
WHERE DEFINER='edac@localhost';


SELECT ROUTINE_NAME, ROUTINE_TYPE, ROUTINE_DEFINITION, DEFINER FROM ROUTINES
WHERE DEFINER='edac@localhost' AND ROUTINE_TYPE='FUNCTION' \G


Error Handling


ALTER TABLE dept ADD PRIMARY KEY (deptno);

INSERT INTO dept VALUES (10, 'MARKETING', 'DELHI');

DROP TABLE departments;
-- ERROR 1051 (42S02): Unknown table 'dacdb.departments'

Cursor


DECLARE v_cur CURSOR FOR select_statement;

Cursor is used to read records one by one from a SELECT query.


Cursor syntax
 DECLARE v_flag INT DEFAULT 0;
 DECLARE CONTINUE HANDLER FOR NOTFOUND SET v_flag = 1;
 DECLARE v_cur CURSOR FOR SELECT ...;
 OPEN v_cur;
 label: LOOP
 	FETCH v_cur INTO variables;
 	IF v_flag = 1 THEN
 		LEAVE label;
 	END IF;
 	
 END LOOP;
 CLOSE v_cur;


