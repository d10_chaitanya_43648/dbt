# Joins

## Cross Join

## Inner Joins

## Left Outer Join

SELECT e.ename, d.dname FROM emps e
LEFT OUTER JOIN depts d ON e.deptno = d.deptno;

## Right Outer Join

SELECT e.ename, d.dname FROM emps e
RIGHT OUTER JOIN depts d ON e.deptno = d.deptno;

## Full Outer Join

SELECT e.ename, d.dname FROM emps e
FULL OUTER JOIN depts d ON e.deptno = d.deptno;
``errorr

## Set Operators
UNION

(SELECT e.ename, d.dname FROM emps e
LEFT OUTER JOIN depts d ON e.deptno = d.deptno)
UNION
(SELECT e.ename, d.dname FROM emps e
RIGHT OUTER JOIN depts d ON e.deptno = d.deptno);

UNION ALL

(SELECT e.ename, d.dname FROM emps e
LEFT OUTER JOIN depts d ON e.deptno = d.deptno)
UNION ALL
(SELECT e.ename, d.dname FROM emps e
RIGHT OUTER JOIN depts d ON e.deptno = d.deptno);

## Self Join

SELECT e.ename, m.ename AS mname FROM emps e
INNER JOIN emps m ON e.mgr = m.empno;


SELECT e.ename, m.ename AS mname FROM emps e
LEFT OUTER JOIN emps m ON e.mgr = m.empno;

## Joins with other clauses

SELECT e.ename, d.dname, a.tal FROM emps e
INNER JOIN depts d ON e.deptno = d.deptno
INNER JOIN addr a ON e.empno = a.empno;

SELECT e.ename, d.dname, a.tal FROM emps e
LEFT OUTER JOIN depts d ON e.deptno = d.deptno
INNER JOIN addr a ON e.empno = a.empno;

SELECT e.ename, d.dname FROM emps e
INNER JOIN depts d ON e.deptno = d.deptno
WHERE e.empno IN (2,3);


